{
  description = "My NixOS Configuration(s)";

  inputs = {
    unstable.url = "github:nixos/nixpkgs/nixpkgs-unstable";

    leading.url = "github:nixos/nixpkgs";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "unstable";
    };

    cornelis.url = "github:mangoiv/cornelis";

    # simplex has no cache and needs a shitton cpu time to build
    simplex-chat.url = "github:simplex-chat/simplex-chat";

    mercury = {
      url = "github:Mercury-Language/mercury";
      flake = false;
    };

    adhoc-ghc.url = "gitlab:mangoiv/adhoc-ghc-shell/main";
  };

  outputs = inputs @ {
    self,
    home-manager,
    unstable,
    leading,
    cornelis,
    ...
  }: let
    system = "x86_64-linux";

    leadingPkgs = import leading {
      inherit system;
    };

    unstablePkgs = import unstable {
      inherit system;
    };

    cornelis-package = cornelis.packages.${system};

    simplex-chat = inputs.simplex-chat.packages.${system}."exe:simplex-chat";

    machines = ["desktop-nixos" "p14-nixos"];
  in
    with unstable.lib; {
      formatter.${system} = unstablePkgs.alejandra;

      packages."${system}" = unstablePkgs;

      nixosConfigurations = genAttrs machines (
        machine:
          nixosSystem {
            inherit system;
            modules = [
              ./modules/systemspecific/generic_configuration.nix
              ./modules/systemspecific/${machine}/hw.nix
              ./modules/systemspecific/${machine}/hardware-configuration.nix
              home-manager.nixosModule
              {
                home-manager = {
                  extraSpecialArgs = {
                    inherit leadingPkgs simplex-chat cornelis-package inputs;
                    ghcDevShell = inputs.adhoc-ghc.devShells.${system}.default.buildInputs;
                  };
                  useGlobalPkgs = true;
                  useUserPackages = true;
                  users.mangoiv =
                    import ./modules/home/${machine}/home.nix;
                };
              }
            ];
          }
      );
    };
}
