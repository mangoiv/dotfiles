{
  config,
  pkgs,
  cornelis-package,
  inputs,
  ...
}: let
  rose-pine-theme = pkgs.vimUtils.buildVimPlugin {
    pname = "rose-pine-neovim";
    version = "unknown";
    src = pkgs.fetchFromGitHub {
      owner = "rose-pine";
      repo = "neovim";
      rev = "32fc6d4a54707fba312f597c0f1cb4f909494659";
      sha256 = "1g7qxkj2yn428wa69d1w1r85l449hbx67zy9a42n48mlmwba0vih";
    };
  };

  mercury-vim = pkgs.vimUtils.buildVimPlugin {
    pname = "mercury-vim";
    version = "unknown";
    src = "${inputs.mercury}/vim";
  };

  cornelis-vim = let
    cp = cornelis-package.cornelis-vim;
  in
    cp
    // {
      pname = cp.name;
      version = "unknown";
    };
in {
  home.file."haskell.vim" = {
    source = ./haskell.vim;
    target = ".config/nvim/syntax/haskell.vim";
    force = true;
  };

  programs.neovim = {
    enable = true;

    package = pkgs.neovim-unwrapped;

    plugins = with pkgs.vimPlugins; [
      cornelis-vim
      Coqtail
      Rename
      cmp-nvim-lsp
      cmp_luasnip
      coq-vim
      direnv-vim
      dracula-vim
      ferret
      fzf-vim
      fzf-lsp-nvim
      gitsigns-nvim
      haskell-vim
      idris2-vim
      lazygit-nvim
      lualine-nvim
      luasnip
      lualine-lsp-progress
      mercury-vim
      nerdtree
      nim-vim
      nvim-cmp
      nvim-lspconfig
      purescript-vim
      rose-pine-theme
      vim-devicons
      vim-hoogle
      vim-lsc
      vim-mergetool
      vim-nix
      vim-repeat
      vim-surround
      vim-vsnip
      vimspector
      vimtex
    ];

    extraConfig = builtins.readFile ./vimrc;

    extraLuaPackages = [pkgs.lua51Packages.luautf8];

    extraPackages = with pkgs; [
      cornelis-package.cornelis-ghc8107
      elmPackages.elm
      elmPackages.elm-language-server
      elmPackages.elm-test
      haskellPackages.fourmolu
      haskellPackages.hls-eval-plugin
      haskellPackages.hls-fourmolu-plugin
      haskellPackages.hls-hlint-plugin
      haskellPackages.hls-splice-plugin
      haskellPackages.hls-tactics-plugin
      jdk
      jdk8
      jdk11
      latexrun
      lazygit
      metals
      nixfmt
      nodejs
      nodePackages.eslint
      ormolu
      rnix-lsp
      scalafmt
    ];

    extraPython3Packages = ps: with ps; [];

    withNodeJs = true;
    withPython3 = true;
    vimdiffAlias = true;
    vimAlias = true;
  };
}
