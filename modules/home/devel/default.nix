{config, ...}: {
  imports = [./ghci ./vscode ./nvim ./kakoune ./helix];
  home.file.".cabal/config" = {
    text = ''
      program-default-options
        ghc-options: -haddock
    '';
  };

  home.file.".agda/defaults" = {
    text = ''
      standard-library
    '';
  };

  programs = {
    direnv = {
      enable = true;
      nix-direnv = {
        enable = true;
      };
    };
    bash.enable = true;
    gitui.enable = true;
  };
}
