{config, ...}: {
  home.file = {
    ".haskeline" = {
      source = ./haskeline;
    };
    ".ghci" = {
      source = ./ghci-conf;
    };
  };
}
