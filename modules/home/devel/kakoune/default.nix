{
  config,
  pkgs,
  ...
}: {
  programs.kakoune = {
    enable = true;
    extraConfig = ''
      eval %sh{kak-lsp --kakoune -s $kak_session}  # Not needed if you load it with plug.kak.
      hook global WinSetOption filetype=(haskell|nix) %{
        lsp-enable-window
      }
      hook global WinSetOption filetype=(asciidoc|fountain|html|latex|markdown) %{
        require-module pandoc
      }

      map global user l %{: enter-user-mode lsp<ret>} -docstring "LSP mode"
      map global user f ': fzf-mode<ret>' -docstring "fzf mode"
      map global normal <c-p> ' fzf-mode<ret>'
    '';
    plugins = with pkgs; [
      kakounePlugins.kak-fzf
      kakounePlugins.pandoc-kak
      kakounePlugins.quickscope-kak
      kakounePlugins.auto-pairs-kak
      kak-lsp
    ];
    config = {
      colorScheme = "rose-pine";
      indentWidth = 2;
      numberLines = {
        enable = true;
        relative = true;
      };
      ui = {
        enableMouse = true;
        assistant = "cat";
      };
      tabStop = 2;
    };
  };

  home.file.".config/kak/colors" = {
    source = ./colors;
    recursive = true;
    force = true;
  };
}
