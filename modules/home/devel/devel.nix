{
  pkgs,
  ghcDevShell,
  ...
}: let
  haskellPkgs = with pkgs.haskellPackages; [
    stack
    brittany
    cabal2nix
    haskell-ci
    hoogle
    implicit-hie
    nix-tree
    ghcid
  ];

  elmPkgs = with pkgs.elmPackages; [elm elm-test elm-live];

  nimPkgs = with pkgs.nimPackages; [nim];

  guiPkgs = with pkgs; [
    dia
    # kicad
    octave
  ];

  idris2Pkgs = with pkgs; [idris2];

  agdaPkgs = with pkgs; [agda];

  compilers = with pkgs; [gcc scryer-prolog elmPackages.elm mercury];

  utilPkgs = with pkgs; [gh direnv elm2nix nimlsp fzf ripgrep delta manix];

  hardwarePkgs = with pkgs; [blackmagic openocd];
in {develPkgs = builtins.concatLists [ghcDevShell haskellPkgs elmPkgs guiPkgs compilers utilPkgs nimPkgs idris2Pkgs agdaPkgs];}
