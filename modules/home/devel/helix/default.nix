{
  config,
  leadingPkgs,
  ...
}: {
  programs.helix = {
    enable = true;
    package = leadingPkgs.helix;
    languages = [
      {
        # purescript language support for helix
        name = "purescript";
        scope = "source.purs";
        injection-regex = "^purs$";
        file-types = ["purs"];
        comment-token = "--";
        roots = ["spago.dhall"];
        auto-format = true;
        indent = {
          tab-width = 2;
          unit = "  ";
        };
        language-server = {
          command = "purescript-language-server";
          args = ["--stdio"];
        };
      }
      {
        name = "haskell";
        file-types = ["hs" "purs"];
        auto-format = true;
      }
    ];
    settings = {
      theme = "dracula";
      editor = {
        lsp.display-messages = true;
        cursor-shape.insert = "underline";
        line-number = "relative";
      };
    };
  };
}
