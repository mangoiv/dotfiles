{pkgs, ...}: {
  programs.vscode = {
    enable = true;
    package = pkgs.vscodium;
    extensions = with pkgs.vscode-extensions; [
      dbaeumer.vscode-eslint
      haskell.haskell
      jnoortheen.nix-ide
      justusadam.language-haskell
      vscodevim.vim
      redhat.java
      brettm12345.nixfmt-vscode
      ms-dotnettools.csharp
      mvllow.rose-pine
      dracula-theme.theme-dracula
    ];

    userSettings = {
      "editor.fontFamily" = "'Jetbrains Mono', 'monospace', monospace, 'Droid Sans Fallback'";
      "editor.fontLigatures" = true;
      "editor.fontSize" = 12;
      "editor.tabSize" = 2;
      "files.autoSave" = "afterDelay";
      "workbench.colorTheme" = "Dracula";
      "workbench.preferredDarkColorTheme" = "Dracula";
      "workbench.preferredLightColorTheme" = "Rosé Pine Dawn";
    };
  };
}
