{
  pkgs,
  simplex-chat,
  ...
}: let
  deezer = pkgs.callPackage ./deezer.nix {};

  guiPkgs = with pkgs; [
    anki
    bitwarden
    drawio
    deezer
    element-desktop
    gnvim
    inkscape
    krita
    logseq
    libreoffice
    mozillavpn
    mumble
    pamixer
    rmview
    simplescreenrecorder
    #ungoogled-chromium
    vlc
    zettlr
  ];

  lookFeelPkgs = with pkgs; [
    comfortaa
    i3lock
    ipafont
    jetbrains-mono
    material-icons
    nerdfonts
    nitrogen
    numix-cursor-theme
  ];

  otherPkgs = with pkgs; [
    bitwarden-cli
    bottom
    brightnessctl
    calc
    calcurse
    cmatrix
    duf
    fzf
    thefuck
    go-2fa
    gopher
    haskellPackages.Monadoro
    hut
    hunspell
    hunspellDicts.de_DE
    hunspellDicts.en_GB-large
    hunspellDicts.en_US-large
    libnotify
    mozwire
    neofetch
    nextcloud-client
    nix-prefetch-git
    nix-prefetch-github
    nix-top
    pandoc
    rmapi
    skim
    simplex-chat
    sl
    taskwarrior-tui
    texlive.combined.scheme-medium
    tree
    unzip
    wakatime
    yubikey-manager
    yubikey-manager-qt
    zip
    zulip
  ];
in {stdPkgs = guiPkgs ++ lookFeelPkgs ++ otherPkgs;}
