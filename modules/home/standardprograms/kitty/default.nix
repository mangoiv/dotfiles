{myConfig, ...}: {
  # Kitty
  programs.kitty = {
    enable = true;
    settings = {
      scrollback_lines = 10000;
      enable_audio_bell = false;
      cursor_shape = "underline";
      window_padding_width = 3;
      confirm_os_window_close = 0;
    };
    extraConfig = builtins.readFile ./dracula.conf;
    # extraConfig = builtins.readFile ./rose-pine.conf;
    font = {
      name = "JetBrainsMono Nerd Font Mono";
      size = 15;
      package = myConfig.font;
    };
  };
}
