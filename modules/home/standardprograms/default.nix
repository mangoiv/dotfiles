{
  pkgs,
  myConfig,
  ...
}: {
  imports = [(import ./kitty {inherit myConfig;})];

  # qt
  qt = {
    enable = true;
    platformTheme = "gtk";
    style.package = pkgs.qtstyleplugins;
    style.name = "gtk2";
  };

  # Zathura
  programs.zathura = with myConfig.colorscheme.scheme; {
    enable = true;
    options = {
      recolor = true;
      completion-bg = bg;
      completion-fg = magenta;
      default-bg = light_bg;
      default-fg = blue;
      highlight-fg = fg;
      inputbar-bg = bg;
      inputbar-fg = cyan;
      statusbar-bg = bg;
      statusbar-fg = blue;
      selection-clipboard = "clipboard";
      font = "Jetbrains Mono 9";
      recolor-lightcolor = bg;
      recolor-darkcolor = fg;
    };
  };

  # htop
  programs.htop = {
    enable = true;
    settings = {
      show_cpu_frequency = true;
      tree_view = true;
    };
  };

  # Zsh
  programs.zsh = {
    enable = true;
    oh-my-zsh = {
      enable = true;
      plugins = ["git" "sudo"];
      theme = "norm";
    };

    shellGlobalAliases = {
      kalm = "cal -m3";
      dict = "dict -h localhost -f";
      gradlew = "./gradlew";
    };
    initExtra = ''eval "$(direnv hook zsh)"'';

    plugins = [
      {
        name = "zsh-autosuggestions";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-autosuggestions";
          rev = "v0.4.0";
          sha256 = "0z6i9wjjklb4lvr7zjhbphibsyx51psv50gm07mbb0kj9058j6kc";
        };
      }
      {
        name = "zsh-syntax-highlighting";
        src = pkgs.fetchFromGitHub {
          owner = "zsh-users";
          repo = "zsh-syntax-highlighting";
          rev = "0e1bb14452e3fc66dcc81531212e1061e02c1a61";
          sha256 = "09ncmyqlk9a3h470z0wgbkrznb5zyc9dj96011wm89rdxc1irxk2";
        };
      }
      {
        name = "zsh-nix-shell";
        file = "nix-shell.plugin.zsh";
        src = pkgs.fetchFromGitHub {
          owner = "chisui";
          repo = "zsh-nix-shell";
          rev = "v0.5.0";
          sha256 = "0za4aiwwrlawnia4f29msk822rj9bgcygw6a8a6iikiwzjjz0g91";
        };
      }
    ];
  };

  programs.bat.enable = true;

  programs.taskwarrior = {
    enable = true;
    dataLocation = "$HOME/Documents/tasks";
  };

  programs.keychain.enable = true;
}
