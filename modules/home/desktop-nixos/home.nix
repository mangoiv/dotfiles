{
  config,
  pkgs,
  lib,
  ...
}: let
  myConfig = {
    font = pkgs.jetbrains-mono;
    icons = pkgs.tela-icon-theme;
    colorscheme = {
      name = "dracula";
      scheme = (import ../colorschemes {inherit pkgs;}).dracula;
    };
  };

  devel = import ../devel/devel.nix {inherit pkgs;};
  inherit
    (import ../standardprograms/standardpackages {inherit pkgs;})
    stdPkgs
    ;
in
  with devel; {
    imports = [../i18 ../devel (import ../standardprograms {inherit pkgs myConfig;})];

    programs.home-manager.enable = true;

    home.username = "mangoiv";
    home.homeDirectory = "/home/mangoiv";

    # all the services
    services = {
      # emacs service
      emacs.enable = false;

      # flameshot
      flameshot.enable = true;

      # gnome-keyring, Probably don't need it
      gnome-keyring = {
        enable = true;
        components = ["secrets" "ssh" "pkcs11"];
      };

      # automatic screenlocking
      screen-locker = {
        enable = false;
        lockCmd = "${pkgs.i3lock}/bin/i3lock -n -c 282a36";
      };

      # udiskie automount, probably not working but I guess it doesn't hurt
      udiskie.automount = {
        enable = true;
        automount = true;
      };

      # picom stuff
      picom = {
        enable = true;
        fade = true;
        fadeDelta = 4;
        shadow = true;
        inactiveDim = "0.1";
      };

      # dunst
      dunst = {
        enable = true;
        iconTheme = {
          name = "Tela";
          package = myConfig.icons;
        };
        settings = with myConfig.colorscheme.scheme; {
          global = {
            geometry = "0x4+15+25";
            transpareny = 15;
            separator_height = 1;
            padding = 8;
            horizontal_padding = 10;
            frame_color = blue;
            separator_color = "frame";
            markup = "full";
            frame_width = 1;
            corner_radius = 5;
            format = "<u>%s</u>\\n%b";
            max_icon_size = 32;
          };

          urgency_low = {
            background = black;
            foreground = fg;
            timeout = 10;
          };

          urgency_normal = {
            background = bg;
            foreground = blue;
            timeout = 10;
          };
          urgency_critical = {
            background = red;
            foreground = fg;
            timeout = 0;
          };
        };
      };
    };

    # GTK
    gtk = {
      enable = true;
      iconTheme = {
        name = "Tela";
        package = myConfig.icons;
      };
      theme = {
        name = "Dracula";
        package = myConfig.colorscheme.scheme.gtk-theme;
      };
    };

    home = {
      pointerCursor = {
        x11.enable = true;
        package = pkgs.dracula-theme;
        name = "Dracula-cursors";
        size = 8;
      };
      # activation = "xmonad --recompile && xmonad --restart";

      # Xmonad
      file.".xmonad" = {
        source = ../wmspecific/xmonad;
        recursive = true;
        onChange = "xmonad --recompile && xmonad --restart";
        force = true;
      };

      # extra packages for me
      packages = stdPkgs ++ develPkgs;
    };

    # don't change, like .. ever
    home.stateVersion = "21.03";
  }
