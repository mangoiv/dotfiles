{
  config,
  pkgs,
  lib,
  simplex-chat,
  ghcDevShell,
  ...
}: let
  myConfig = {
    font = pkgs.jetbrains-mono;
    icons = pkgs.tela-icon-theme;
    colorscheme = {
      name = "dracula";
      scheme = (import ../colorschemes {inherit pkgs;}).dracula;
    };
    #colorscheme = {
    #  name = "dracula";
    #  scheme = (import ../colorschemes).dracula;
    #};
  };

  devel = import ../devel/devel.nix {inherit pkgs ghcDevShell;};
  inherit
    (import ../standardprograms/standardpackages {inherit pkgs simplex-chat;})
    stdPkgs
    ;
in
  with devel; {
    imports = [../i18 ../devel (import ../standardprograms {inherit pkgs myConfig;})];

    programs.home-manager.enable = true;

    home.username = "mangoiv";
    home.homeDirectory = "/home/mangoiv";

    # all the services
    services = {
      # emacs service
      emacs.enable = false;

      # flameshot
      flameshot.enable = true;

      # gnome-keyring, Probably don't need it
      gnome-keyring = {
        enable = true;
        components = ["secrets" "ssh" "pkcs11"];
      };

      # automatic screenlocking
      screen-locker = {
        enable = true;
        lockCmd = "${pkgs.i3lock}/bin/i3lock -n -c #44475a";
        inactiveInterval = 60;
      };

      # stalone tray
      stalonetray = {
        enable = true;
        config = {
          background = myConfig.colorscheme.scheme.bg;
          decorations = null;
          geometry = "3x1-600+0";
          icon_size = 30;
          sticky = true;
        };
      };

      # udiskie automount, probably not working but I guess it doesn't hurt
      udiskie.automount = {
        enable = true;
        automount = true;
      };

      # picom stuff
      picom = {
        enable = true;
        vSync = true;
        fade = true;
        fadeDelta = 4;
        shadow = true;
      };

      # unclutter to hide the mouse when not needed
      unclutter.enable = true;

      # dunst
      dunst = {
        enable = true;
        iconTheme = {
          name = "Tela";
          package = pkgs.tela-icon-theme;
        };
        settings = with myConfig.colorscheme.scheme; {
          global = {
            geometry = "0x4+15+25";
            transpareny = 15;
            separator_height = 1;
            padding = 8;
            horizontal_padding = 10;
            frame_color = bg;
            separator_color = "frame";
            markup = "full";
            frame_width = 1;
            corner_radius = 5;
            format = "<u>%s</u>\\n%b";
            max_icon_size = 32;
          };
          urgency_low = {
            background = bg;
            foreground = fg;
            timeout = 10;
          };

          urgency_normal = {
            background = bg;
            foreground = fg;
            timeout = 10;
          };
          urgency_critical = {
            background = bg;
            foreground = red;
            timeout = 0;
          };
        };
      };
    };

    # GTK
    gtk = {
      enable = true;
      iconTheme = {
        name = "Tela";
        package = pkgs.tela-icon-theme;
      };
      theme = {
        name = "Dracula";
        package = myConfig.colorscheme.scheme.gtk-theme;
      };
    };

    home = {
      # activation = "xmonad --recompile && xmonad --restart";
      pointerCursor = {
        x11.enable = true;
        package = pkgs.dracula-theme;
        name = "Dracula-cursors";
        size = 12;
      };

      # Xmonad
      file.".xmonad" = {
        source = ../wmspecific/xmonad;
        recursive = true;
        onChange = "xmonad --recompile && xmonad --restart";
        force = true;
      };

      # extra packages for me
      packages = stdPkgs ++ develPkgs;
    };

    # don't change, like .. ever
    home.stateVersion = "21.03";
  }
