{pkgs, ...}: {
  dracula = {
    fg = "#f8f8f2";
    bg = "#282a36";
    light_bg = "#44475a";

    url_color = "#ffb86c";

    black = "#21222c";
    black_alt = "#6272a4";

    red = "#ff5555";
    red_alt = "#ff6e6e";

    green = "#50fa7b";
    green_alt = "#69ff94";

    yellow = "#f1fa8c";
    yellow_alt = "#ffffa5";

    blue = "#bd93f9";
    blue_alt = "#d6acff";

    magenta = "#ff79c6";
    magenta_alt = "#ff92df";

    cyan = "#8be9fd";
    cyan_alt = "#a4ffff";

    white = "#f8f8f2";
    white_alt = "#ffffff";

    gtk-theme = pkgs.dracula-theme;
  };

  rose-pine = {
    fg = "#e0def4";
    bg = "#191724";
    light_bg = "#2a2837";

    url_color = "#c4a7e7";

    # black
    black = "#26233a";
    black_alt = "#6e6a86;";

    # red
    red = "#eb6f92";
    red_alt = "#eb6f92";

    # green
    green = "#31748f";
    green_alt = "#31748f";

    # yellow
    yellow = "#f6c177";
    yellow_alt = "#f6c177";

    # blue
    blue = "#9ccfd8";
    blue_alt = "#9ccfd8";

    # magenta
    magenta = "#c4a7e7";
    magenta_alt = "#c4a7e7";

    # cyan
    cyan = "#ebbcba";
    cyan_alt = "#ebbcba";

    # white
    white = "#e0def4";
    white_alt = "#e0def4";

    gtk-theme = pkgs.rose-pine-gtk-theme;
  };
}
