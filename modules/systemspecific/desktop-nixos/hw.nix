{
  config,
  pkgs,
  lib,
  ...
}: {
  boot = {
    kernelPackages = pkgs.linuxPackages_5_10;
    initrd.luks.devices = {
      root = {
        device = "/dev/disk/by-uuid/2aabd27b-c672-4c9f-8ce5-dbff3d277aca";
        preLVM = true;
        allowDiscards = true;
      };
    };

    initrd.network.ssh = {
      enable = true;
      authorizedKeys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDfh5GZ9Hyc0QR1300IF9A4kH1Im6bbNEZKBBY1evLK1 github/-lab"
        "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCytdFMnKMg4NQhq7Y+LSoF8NpgTsnwhsD1SLwD1Ji2dEdClx/fDrtqQ5Y59pHyvHxvze3IQ7+E5vzb6Z5YZFP9DCMP5iL61OhQGw0Sjs9BfYlvHmNEA3v9PQZ7MzlD3fcPHUL+tnFikNtqlF1w5UPn36ceH4q0jDFHr1sRdrozf4/Ndv9HDe0erZfjjaqgGExwvm2JYEKN6F+87kOdZkelEkwh6Ux90ineFnvtAye4ZT1GCD+HZZdQ6BIQxvnylZaYSRkqDLy5GDcJ2Yiom54fYIbtGcWjGM2UUedT9DUEf3IcPTYhIER63MZIBhOIP/nInwGLYZfY+Tndtdr0Y2zLlsW4iEiR0SG1xow37T+zUxMU7zHBcRWNoY76QdgvJZ4PN5EGehkNuh3EP8Jr2XA9Ln8wMjH7IzTtabcyFH5NXSDkgoBENxEbUnsKHh/XHCMe2HsylZ1J+IPynCYwTeYOjKn+8aYLmIr1yORBIEyozL0WRythU5Re/xbz3PYjh4tYzzQgb0EhpjUF5pEjCVKVQYWhezbzfT9kXUeXdk3RRmG8btPP+SVeKWxdkwMVTrn61loNHYad4LDLXCfDaQsB0rDfbDnx00XH1sSmhbxwoivJT+o8UsoxOzniOG6g4BP8j4QDgQTWWdig2ITM8Wd8U3dX8xnkyQo68KN16HdlHw== root@t14-nixos"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINT1LMUphoRAn1EPro0DuAdzTDwmM/JPGVM6aBoKPsfN root@t14-nixos"
      ];
    };
    loader.grub.useOSProber = true;
  };

  programs.steam.enable = true;

  users.users.marietta = {
    isNormalUser = true;
  };

  services = {
    xserver = {
      videoDrivers = [
        "nvidia"
      ];

      xrandrHeads = [
        {
          output = "HDMI-0";
          primary = true;
          monitorConfig = ''
            Option "Position" "0 297"
          '';
        }
        {
          output = "HDMI-1";
          monitorConfig = ''
            Option "Position" "2560 0"
            Option "PreferredMode" "1920x1080_60.00"
            Option "Rotate" "Left"
            Option "RightOf" "HDMI-1"
          '';
        }
      ];
    };

    openssh = {
      enable = true;
      forwardX11 = true;
    };
  };

  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "nvidia-x11"
      "nvidia-settings"
      "steam"
      "steam-original"
      "steam-runtime"
    ];

  # hostname
  networking.hostName = "desktop-nixos";

  networking.firewall.allowedTCPPorts = [5900];

  networking.firewall.allowedUDPPorts = [5901];
  system.stateVersion = "21.05";
}
