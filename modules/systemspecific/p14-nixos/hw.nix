# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  ...
}: {
  boot = {
    kernelPackages = pkgs.linuxPackages_latest;

    kernelParams = ["amdgpu.backlight=0" "acpi_backlight=none"];
    kernelModules = ["acpi_call"];

    initrd.luks.devices = {
      root = {
        device = "/dev/disk/by-uuid/3454bc1e-4b2a-4703-b725-341629745303";
        preLVM = true;
        allowDiscards = true;
      };
    };
  };

  hardware.video.hidpi.enable = true;
  services = {
    xserver.xrandrHeads = [
      {
        output = "eDP";
        primary = true;
        monitorConfig = ''
          DisplaySize 620 348
        '';
      }
      {
        output = "DisplayPort-3";
        monitorConfig = ''
          Option "RightOf" "eDP"
        '';
      }
      {
        output = "DisplayPort-2";
        monitorConfig = ''
          Option "RightOf" "DisplayPort-3"
          Option "Rotate" "left"
        '';
      }
    ];

    fprintd.enable = true;
  };

  # hostname
  networking.hostName = "p14-nixos";
}
