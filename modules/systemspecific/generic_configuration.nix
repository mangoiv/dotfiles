# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).
{
  config,
  pkgs,
  lib,
  ...
}: {
  # everything booting
  boot = {
    loader = {
      grub.enable = true;
      grub.version = 2;
      grub.device = "nodev";
      grub.efiSupport = true;
      efi.canTouchEfiVariables = true;
    };
  };

  # prepare the nix-flakes experimental feature for installation
  nix = {
    package = pkgs.nixVersions.unstable;

    optimise.automatic = true;

    gc = {
      automatic = true;
      options = ''
        --delete-older-than 14d
      '';
    };

    registry = {
    };

    extraOptions = ''
      keep-outputs = true
      keep-derivations = true
      experimental-features = nix-command flakes
    '';

    # Binary Cache for Haskell.nix

    settings = {
      trusted-users = ["root" "@wheel"];
      substituters = [
        https://cache.nixos.org/ # nixos cache
        https://hydra.iohk.io # iog hydra cache
        https://iohk.cachix.org
        https://cache.nixos.org/ # iog cachix cache
        https://public-plutonomicon.cachix.org # plutonomicon cache
        https://nickel.cachix.org # nickel cachix
      ];
      trusted-public-keys = [
        cache.nixos.org-1:6NCHdD59X431o0gWypbMrAURkbJ16ZPMQFGspcDShjY= # nixos pubkey
        hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ= # iog hydra pubkey
        iohk.cachix.org-1:DpRUyj7h7V830dp/i6Nti+NEO2/nhblbov/8MW7Rqoo= # iog cachix key
        public-plutonomicon.cachix.org-1:3AKJMhCLn32gri1drGuaZmFrmnue+KkKrhhubQk/CWc= # plutonomicon cachix key
        nickel.cachix.org-1:ABoCOGpTJbAum7U6c+04VbjvLxG9f0gJP5kYihRRdQs= # nickel cachix key
      ];
    };
  };

  # everything networking
  networking = {
    networkmanager.enable = true;
  };

  time.timeZone = "Europe/Berlin";

  location.latitude = 51.22;
  location.longitude = 12.37;

  powerManagement.enable = true;

  sound.enable = true;

  services = {
    udev.packages = [pkgs.yubikey-personalization];
    pcscd.enable = true;
    mozillavpn.enable = true;

    openssh.enable = true;

    blueman.enable = true;

    printing.enable = true;
    fprintd = {
      enable = true;
    };

    dictd = {
      enable = true;
      DBs = with pkgs.dictdDBs; [
        wiktionary
        wordnet
        eng2deu
        deu2eng
      ];
    };

    redshift = {
      enable = true;
      temperature.day = 5500;
      temperature.night = 3700;
    };

    # powermanagement
    tlp.enable = !(config.services.xserver.desktopManager.gnome.enable);
    upower.enable = true;

    #dbus
    dbus = {
      enable = true;
    };

    #xserver configuration
    xserver = {
      enable = true;
      layout = "us";
      #touchpad
      libinput.enable = true;

      #xmonad
      windowManager.xmonad = {
        enable = true;
        enableContribAndExtras = true;
      };

      #lightdm and autologin
      displayManager = {
        defaultSession = "none+xmonad";
        lightdm.enable = true;
        autoLogin.enable = true;
        autoLogin.user = "mangoiv";
      };

      desktopManager.gnome = {
        enable = true;
      };
    };
  };

  systemd.services.upower.enable = true;

  hardware = {
    gpgSmartcards.enable = true;
    pulseaudio.enable = true;
    bluetooth = {
      enable = true;
      package = pkgs.bluez;
      powerOnBoot = true;
    };
  };

  # set up my user
  users.users.mangoiv = {
    shell = pkgs.zsh;
    isNormalUser = true;
    home = "/home/mangoiv";
    description = "Magnus Viernickel";
    extraGroups = ["docker" "wheel" "networkmanager" "video"]; # Enable ‘sudo’ for the user.
  };

  environment.systemPackages = with pkgs; [
    binutils
    cabal-install
    cachix
    curl
    dmenu
    dunst
    emacs
    fd
    firefox
    gcc
    ghc
    haskellPackages.xmobar
    haskellPackages.xmonad
    haskellPackages.xmonad-contrib
    htop
    imv
    jetbrains-mono
    kitty
    pamixer
    pavucontrol
    pinentry
    pulseaudio-ctl
    signal-desktop
    thunderbird
    vim
    wget
  ];

  virtualisation = {
    docker.enable = true;
    podman.enable = true;
  };

  programs.steam.enable = true;

  nixpkgs.config.allowUnfreePredicate = pkg:
    builtins.elem (lib.getName pkg) [
      "steam"
      "steam-original"
      "steam-runtime"
      "deezer-desktop"
    ];

  programs.dconf.enable = true;

  programs.neovim.defaultEditor = true;

  programs.gnupg = {
    agent = {
      enable = true;
      enableSSHSupport = true;
      pinentryFlavor = "tty";
    };
  };

  programs.git = {
    enable = true;
    config = {
      init.defaultBranch = "main";
    };
  };

  programs.thunar = {
    enable = true;
    plugins = [
      pkgs.xfce.thunar-archive-plugin
      pkgs.xfce.thunar-volman
    ];
  };

  # security stuff
  security = {
    apparmor.enable = true;
  };
}
